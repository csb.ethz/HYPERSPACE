function [x] = rowfevaln(func, nout, B)
%ROWFEVALN Evaluate func at each row of B collecting each of the first
% nout outputs.
%
    if ischar(func)
        func = str2func(func);
    end
    n = size(B,1);
    x = nan(n,nout);
    xiCell = cell(1,nout);
    % Note: overhead from re-arranging mem to fetch one mem part (column) in
    %       each iteartion (BT = B') is slightly larger than the actual speed
    %       gain
    for i=1:n
        % temp. row var improves loop speed a bit (up to ca. 10% for @sum)
        Bi = B(i,:);
        [xiCell{1:nout}]=func(Bi);
        x(i,:) = cell2mat(xiCell);
    end
end
