function [x] = rowfeval1(func, B)
%ROWFEVAL1 Evaluate func at each row of B collecting first output.
%
    if ischar(func)
        func = str2func(func);
    end
    n = size(B,1);
    x = nan(n,1);
    % Note: overhead from re-arranging mem to fetch one mem part (column) in
    %       each iteartion (BT = B') is slightly larger than the actual speed
    %       gain
    for i=1:n
        % temp. row var improves loop speed a bit (up to ca. 10% for @sum)
        Bi = B(i,:);
        x(i) = func(Bi);
    end
end
