function [varargout] = functionfinal(varargin)
%FUNCTIONFINAL ...
%
% Syntax:
%     functionfinal('S', threshold, n, dim, fun, fargs)
%         setup functionfinal
%     [viable, nviable, cuenta] = functionfinal('G')
%         get persistent data
%     [indicator, cost] = functionfinal(p)
%         evaluate fun(p, fargs{:}) and check threshold
%
    persistent p;

    v1 = varargin{1};
    if isnumeric(v1)
        % assert(~isempty(p));
        p.nfeval = p.nfeval+1;
        try
            cost = p.fun(v1,p.fargs{:});
        catch ME
            warning('FUNCTIONFINAL:CostEvaluationError','Error while evaluating cost function at %s:\n\t%s: %s\n', mat2str(v1), ME.identifier, ME.message);
            cost = Inf;
        end

        isviable = (cost <= p.threshold);
        if isviable
            p.nviable = p.nviable+1;
            p.viable(p.nviable,1:p.dim) = v1; % independent of if v1 is row/col vec
            p.viable(p.nviable,p.dim+1) = cost;
        end
        indicator = double(isviable);
        varargout{1} = indicator;
        varargout{2} = cost;
    else
        switch v1
        case '-s' % setup persistent data
            p.nfeval = 0;
            p.threshold = varargin{2};
            n = varargin{3};
            p.dim = varargin{4};
            p.viable = initviable(n,p.dim);
            p.nviable = 0;
            p.fun = varargin{5};
            if ischar(p.fun)
                p.fun = str2func(p.fun);
            end
            p.fargs = varargin{6};
        case '-g' % get persistent data
            0;
        otherwise % evaluate fun using persistent data
            error('FUNCTIONFINAL:InvalidSwitch','Invalid switch: ''%s''.',v1);
        end
        varargout{1} = p.viable;
        varargout{2} = p.nviable;
        varargout{3} = p.nfeval;
    end

end
