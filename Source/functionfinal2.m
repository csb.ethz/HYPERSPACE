function [varargout] = functionfinal2(varargin)
%FUNCTIONFINAL2 Same as functionfinal, but cost is non-zero iff indicator is
% non-zero.
    [varargout{1:nargout}]=functionfinal(varargin{:});
    if isnumeric(varargin{1}) % varargout = { indicator, cost }
        if ~varargout{1} % if indicator == 0 then set cost := 0
            varargout{2} = varargout{1};
        end
    end
end
