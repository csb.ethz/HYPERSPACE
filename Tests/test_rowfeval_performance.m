function [] = test_rowfeval_performance()
% Test problem: row-wise function application
    rep = 100;
    
    m = rand(5e5, 20);
    test_cases = {
        '     cellfun sum', @rowfeval_cellfun,          @sum;
        'row-wise for sum', @rowfeval_rowwise,          @sum;
        'r-w +tmp var sum', @rowfeval_rowwise_tmpvar,   @sum;
        'col-wise for sum', @rowfeval_colwise,          @sum;
        'c-w +tmp var sum', @rowfeval_colwise_tmpvar,   @sum;        
        '    r-w anon sum', @rowfeval_rowwise,          @(x) sum(x);
        '    r-w nest sum', @rowfeval_rowwise,          @nested_sum;
    };
    run_tests(test_cases, rep, m);

    % pre-transpose the matrix
    mT = m';
    test_cases = {
        '    c-w preT sum', @colfeval_colwise,          @sum;
        'c-w preT+tmp sum', @colfeval_colwise_tmpvar,   @sum;        
    };
    run_tests(test_cases, rep, mT);

end

function [] = run_tests(test_cases, rep, m)
    n_test_cases = size(test_cases,1);
    for i=1:n_test_cases
        [test_case_name, feval_func, func] = test_cases{i,:};
        % burn-in (mem access)
        time_feval(4, feval_func, func, m);
        % actual tests
        et = time_feval(rep, feval_func, func, m);
        fprintf('%s: %.3f +/- %.3f sec.\n', test_case_name, mean(et), std(et));
    end
end

function [et] = time_feval(rep, feval_func, func, m)
    for k=rep:-1:1
        tic;
        feval_func(func, m);
        et(k) = toc;
    end
end

function x = rowfeval_cellfun(func, m)
    x = cellfun(func, num2cell(m,2));
end

function x = rowfeval_rowwise(func, m)
    n = size(m,1);
    x = nan(1,n);
    for i=1:n
        x(i) = func(m(i,:));
    end
end

function x = rowfeval_rowwise_tmpvar(func, m)
    n = size(m,1);
    x = nan(1,n);
    for i=1:n
        mi = m(i,:);
        x(i) = func(mi);
    end
end

function x = rowfeval_colwise(func, m)
    n = size(m,1);
    x = nan(1,n);
    mT = m';
    for i=1:n
        x(i) = func(mT(:,i));
    end
end

function x = rowfeval_colwise_tmpvar(func, m)
    n = size(m,1);
    x = nan(1,n);
    mT = m';
    for i=1:n
        mi = mT(:,i);
        x(i) = func(mi);
    end
end

function x = colfeval_colwise(func, m)
    n = size(m,2);
    x = nan(1,n);
    for i=1:n
        x(i) = func(m(:,i));
    end
end

function x = colfeval_colwise_tmpvar(func, m)
    n = size(m,2);
    x = nan(1,n);
    for i=1:n
        mi = m(:,i);
        x(i) = func(mi);
    end
end

function x = nested_sum(x)
    x = sum(x);
end
