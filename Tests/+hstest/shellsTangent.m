function [cost] = shellsTangent( vecOrStruct )
%SHELLSTANGENT Distance to a closer of the two shells at (r_small+r_large)/2:
% first around the origin, and a second tangent to the first one on the right
% in the tp_dim (by default: 1).
%
% First call to configure:
%
%   hstest.shellsTangent( opts )
%
% where opts is a structure with fields:
%
%   r_small (numeric)
%   r_large (numeric)
%   tp_dim  (numeric) optional; default 1
%
% and radiuses are used to configure hstest.shell function.
%
persistent fdat;

if isstruct( vecOrStruct )
        hstest.shell(vecOrStruct);
        fdat = vecOrStruct;
        if ~isfield(fdat,'tp_dim')
            fdat.tp_dim = 1;
        end
        cost = NaN;
else
        x = vecOrStruct;
        c2 = zeros(1,numel(x)); % = c1
        c2(fdat.tp_dim) = 2*fdat.r_large;
        cost = min(hstest.shell(x), hstest.shell(x-c2));
end
end % function
