function [cost] = shell( vecOrStruct )
%SHELL Distance to a shell at (r_small+r_large)/2, around the origin.
%
% First call to configure:
%
%   hstest.shellsTangent( opts )
%
% where opts is a structure with fields:
%
%   r_small (numeric)
%   r_large (numeric)
%
persistent fdat;

if isstruct( vecOrStruct )
    if ~isfield(vecOrStruct,'r_small')
        error('HSTEST:shell:InvalidStructInput', 'Missing r_small field.');
    end
    if ~isfield(vecOrStruct,'r_large')
        error('HSTEST:shell:InvalidStructInput', 'Missing r_large field.');
    end
    if vecOrStruct.r_small >= vecOrStruct.r_large
        error('HSTEST:shell:InvalidNumericInput', 'Outer radius r_large not greater than inner radius r_small.');
    end
    fdat = vecOrStruct;
    cost = NaN;
else
    x = vecOrStruct;
    cost = abs(norm(x)-((fdat.r_small+fdat.r_large)/2));
end
end % function
