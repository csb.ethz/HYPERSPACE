\documentclass{article}
\usepackage[utf8]{inputenc}

\title{Benchmarks: HYPERSPACE v1.3}

\author{Miko{\l}aj Rybi{\'n}ski}
\date{\today}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% PACKAGES
\usepackage{natbib}
\usepackage{graphicx}
\graphicspath{{img/}}
\usepackage{verbatim}
\usepackage{commath}
\usepackage{chngcntr}
\counterwithin{figure}{subsection}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% MACROS


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% DOCUMENT
\begin{document}

\maketitle

\begin{abstract}
HYPERSPACE v1.3 package is able to accurately estimate (from below) all tested volumes within several minutes of running time for the most complex 15 dimensional cases.
\end{abstract}

\section{Introduction}
The three benchmark setups are:
\begin{enumerate}
    \item[\secref{s:donut}] ,,Donut'' --- described in the original paper \citep[][Fig.~6(a)\&(b)]{zamora_2011};
    \item[\secref{s:ddonut}] ,,Double Donut'' --- described in the original paper \citep[][Fig.~6(c)\&(d)]{zamora_2011};
    \item[\secref{s:wurst}] ,,Wurst'' --- see the code for all details (\verb|Tests/AllTest>testIntegrateWurst| and \verb|Tests/+hstest/+wurst/initRandWurst.m|).
\end{enumerate}

\paragraph{Important} There was no fine-tuning of parameters per a~test case. All benchmarks were run using default values for all internal parameters of the methods.

\paragraph{Note} Previously, using version 1.2.1, I've tested few sample size splits among the three integration steps: 1. adaptive out-of-equilibrium MCMC exploration; 2. ellipsoids cover expansions, and 3. MC ellipsoids cover-based sampling (for volume estimation and cost integration). Split giving the best overall results is shown here.


\subsection{How to reproduce}

Run:
\begin{verbatim}
    runtests('AllTest');
\end{verbatim}
with the following adjustments in the \verb|Tests/AllTest.m| file:
\begin{verbatim}
function tests = AllTest()
    ...
    tests = functiontests({@setupOnce, @teardownOnce, @setup, @teardown,...
        @testIntegrateDonut, @testIntegrateDoubleDonut, @testIntegrateWurst});
    ...
end
...
function setupOnce(testCase)
    ...
    % bulk integration: benchmarking setup
    s.dim_vec = [2 5 10 15];
    s.nfeval_vec = [5e4 1e5 2e5 5e5];
    s.volRelErrTol = 0;
    s.nrep = 4;
    s.saveMat = true;
    s.parallelize = false;
    s.nfevalProportionsArray = [...
        4 5 1
    ]; % function evaluations proprotions for MCexp, ELexp and Volint
    testCase.TestData.bulk = s;
    ...
end
\end{verbatim}


\subsection{Setup}

Tests were run in a single CPU setup (MATLAB parallel pool inactive) in the following setup:
\begin{verbatim}
             Name: 'Intel(R) Core(TM) i7-6567U CPU @ 3.30GHz'
            Clock: '3300 MHz'
            Cache: '256 KB'
    NumProcessors: 2
           OSType: 'Mac OS/X'
        OSVersion: '10.13.6
    MATLABVersion: '9.3.0.713579 (R2017b)'
\end{verbatim}



\clearpage
\section{Benchmark results}

\subsection{Donut}\label{s:donut}
\begin{figure}[!h]
\centering
\includegraphics[width=0.8\textwidth]{MEBS_2d_Donut}
\caption{2-D ,,Donut'' sample and ellipsoid cover after running \texttt{OEAMC+MEBS+Volint}.}
\label{fig:donut_2d}
\end{figure}

\begin{figure}[!ht]
\centering
\includegraphics[width=1.0\textwidth]{Donut_451_180903}
\caption{,,Donut'' benchmark results for different dimensions and sample sizes after running \texttt{OEAMC+MEBS+Volint}, with given sample split proportions.}
\label{fig:donut_bench}
\end{figure}



\clearpage
\subsection{Double Donut}\label{s:ddonut}
\begin{figure}[!h]
\centering
\includegraphics[width=0.8\textwidth]{MEBS_2d_DoubleDonut}
\caption{2-D ,,Double Donut'' sample and ellipsoid cover after running \texttt{OEAMC+MEBS+Volint}.}
\label{fig:ddonut_2d}
\end{figure}

\begin{figure}[!ht]
\centering
\includegraphics[width=1.0\textwidth]{DoubleDonut_451_180903}
\caption{,,Double Donut'' benchmark results for different dimensions and sample sizes after running \texttt{OEAMC+MEBS+Volint}, with given sample split proportions.}
\label{fig:ddonut_bench}
\end{figure}


\clearpage
\subsection{Wurst}\label{s:wurst}
\begin{figure}[!h]
\centering
\includegraphics[width=0.8\textwidth]{MEBS_2d_Wurst}
\caption{2-D ,,Wurst'' sample and ellipsoid cover after running \texttt{OEAMC+MEBS+Volint}.}
\label{fig:wurst_2d}
\end{figure}

\begin{figure}[!ht]
\centering
\includegraphics[width=1.0\textwidth]{Wurst_451_180904}
\caption{,,Wurst'' benchmark results for different dimensions and sample sizes after running \texttt{OEAMC+MEBS+Volint}, with given sample split proportions.}
\label{fig:wurst_bench}
\end{figure}


\clearpage
\section{Discussion and Conclusions}
The more complex the viable space shape the more MCMC (OEAMC) samples are needed to get good seeds for the ellipsoids expansion via sampling (MEBS). Once, the ellipsoids fill well the viable volume, integration can be done with relatively few samples (min. tested: 10\%). In particular, for the ,,Donut'' case-study, the 40-50-10 \% sample split was not the best, but it was good-enough to reach under 5\% error in 15 dimensions (with up to 45'000 samples), and it was the best for the most complex ,,Wurst'' case-study.

HYPERSPACE package is able to accurately estimate (from below) all tested volumes. To achieve up to 5\% accuracy in the most complex 15-dimensional ,,Wurst'' case one needs to evaluate the cost function up to 41'500 times. In the performance optimized v1.3 of the package this takes only up to 4m 10s. Most of this time is spent finding minimum-volume enclosing ellipsoids (MVEE) using MATLAB implementation of the original Khachiyan's algorithm~\citep{khachiyan_1996}. The next steps would be to switch to a more efficient method solving the MVEE problem~\citep[][resulting in up to 93--98\% running time reduction]{todd_2007}. Nevertheless, the current running time is already relatively small with respect to time to evaluate what would be a~very fast cost function performing numerical integration of differential equations (with a~fairly complex high-dimensional viable space, assuming as low as 0.01s per cost function evaluation, 50'000 evaluations would take ca. 8m 20s alone).


\bibliographystyle{plain}
\bibliography{references}

\end{document}
